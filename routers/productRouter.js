const express = require('express');
const router = express.Router()

let dataProduct = [
    {
        id : 1,
        namaProduct : "Laptop ROG",
        namaBrand : "Asus",
        jumlahKetersediaan : 900
    },
    {
        id : 2,
        namaProduct : "Thinkpad",
        namaBrand : "Lenovo",
        jumlahKetersediaan : 900
    },
    {
        id : 3,
        namaProduct : "Macbook",
        namaBrand : "Apple",
        jumlahKetersediaan : 900
    },
    {
        id : 4,
        namaProduct : "Thinkbook",
        namaBrand : "Lenovo",
        jumlahKetersediaan : 900
    }
]


router.get('/product', (req, res) => {

    res.status(200).json(dataProduct)
});

router.get('/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct
    filteredProduct = dataProduct.find(x => x.id == idProduct)

    if(filteredProduct == undefined){
        res.status(400).json("Product does no exist")
        return
    }
    res.status(200).json(filteredProduct)
});

router.post('/product', (req, res) => {
    const {namaProduct, namaBrand, jumlahKetersediaan} = req.body

    const nextId = dataProduct.length + 1

    if(namaProduct === undefined || namaBrand === undefined || jumlahKetersediaan === undefined){
        res.status(400).json("Bad request")
        return
    }

    dataProduct.push({
        id: nextId,
        namaProduct: namaProduct,
        namaBrand: namaBrand,
        jumlahKetersediaan: jumlahKetersediaan
    })

    res.status(200).json("Success!")
});

router.put('/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct
    const {namaProduct, namaBrand, jumlahKetersediaan} = req.body

    if(namaProduct === undefined || namaBrand === undefined || jumlahKetersediaan === undefined){
        res.status(400).json("Bad request")
        return
    }

    let isFound = false
    for(var x = 0; x<dataProduct.length; x++){
        if(dataProduct[x].id == idProduct){
            dataProduct[x].namaProduct = namaProduct
            dataProduct[x].namaBrand = namaBrand
            dataProduct[x].jumlahKetersediaan = jumlahKetersediaan

            isFound = true
            break
        }
    }

    if(isFound == false){
        res.status(404).json("Data Product is not found!")
        return
    }else{
        res.status(200).json("Updated!")
        return
    }

});

router.delete('/product/:idProduct', (req, res) => {
    const idProduct = req.params.idProduct

    const searchProduct = dataProduct.find(x => x.id == idProduct)

    if (searchProduct === undefined){
        res.status(404).json("Data Product in not found!")
        return
    }

    const index = dataProduct.indexOf(searchProduct)

    dataProduct.splice(index, 1)

    res.status(200).json("Deleted")
});

module.exports = router