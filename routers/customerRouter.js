const express = require('express')
const router = express.Router()

//CRUD
// Create, Read, Update, Delete

let dataCustomer = [
    {
        id: 1,
        nama: "Dennis Tutuko",
        alamat: "Jakarta"
    },
    {
        id: 2,
        nama: "Qorina Putri",
        alamat: "Bogor"
    },
    {
        id: 3,
        nama: "Rezky Irawan",
        alamat: "Ciledug"
    }    
]

router.post('/api/customer', (req, res) => {
    const {nama, alamat} = req.body

    if(nama === undefined || alamat === undefined){
        res.status(400).json("Bad request because data is not sent")
        return
    }

    const nextId = dataCustomer.length + 1

    dataCustomer.push({
        id : nextId,
        nama: nama,
        alamat: alamat
    })

    res.status(200).json("Success!")
});

router.get('/api/customer', (req, res) => {
    res.status(200).json(dataCustomer)
})

router.get('/api/customer/:idCustomer', (req, res) => {
    const idCustomer = req.params.idCustomer
    const filteredCustomer = dataCustomer.find(x => x.id == idCustomer)

    if(filteredCustomer === undefined){
        res.status(404).json("Data customer is not found!")
        return
    }
    res.status(200).json(filteredCustomer)
});


router.put('/api/customer/:idCustomer', (req, res) => {
    const idCustomer = req.params.idCustomer
    const {nama, alamat} = req.body

    if(nama === undefined || alamat === undefined){
        res.status(400).json("Bad request because some data is not sent")
        return
    }

    let isFound = false
    for(let x = 0; x < dataCustomer.length; x++){
        if(dataCustomer[x].id == idCustomer){
            dataCustomer[x].nama = nama
            dataCustomer[x].alamat = alamat

            isFound = true
            break
        }
    }
    
    
    if(isFound == false){
        res.status(404).json("Data customer is not found!")
        return
    }else{
        res.status(200).json("Updated!")
        return
    }

});

router.delete('/api/customer/:idCustomer', (req, res) => {
    const idCustomer = req.params.idCustomer

    const searchCustomer = dataCustomer.find(x => x.id == idCustomer)

    if (searchCustomer === undefined){
        res.status(404).json("Data customer is not found!")
        return
    }

    const index = dataCustomer.indexOf(searchCustomer)

    dataCustomer.splice(index, 1)

    res.status(200).json("Deleted")
});

module.exports = router